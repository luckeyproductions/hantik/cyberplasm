
#include "blobcam.h"

void BlobCam::RegisterObject(Context* context)
{
    context->RegisterFactory<BlobCam>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

BlobCam::BlobCam(Context* context) : LogicComponent(context)
{
}

void BlobCam::OnNodeSet(Node* node)
{
    if (node)
        CreateView();
}


void BlobCam::CreateView()
{
    const Vector3 blobPos{ GetScene()->GetChild("Blob")->GetWorldPosition() };

    node_->SetPosition(blobPos + Vector3{ -50.0f, 75.0f, -50.0f });
    node_->LookAt(blobPos);
    Camera* camera{ node_->CreateComponent<Camera>() };
    camera->SetFov(80);
    camera->SetFarClip(2000.0f);

    Viewport* viewport{  new Viewport(context_, GetScene(), camera) };
    RenderPath* effectRenderPath{viewport->GetRenderPath()};
    effectRenderPath->Load(GetSubsystem<ResourceCache>()->GetResource<XMLFile>("RenderPaths/Forward.xml"));
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", 0.23f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2(0.9f, 0.1f));
    effectRenderPath->SetEnabled("BloomHDR", true);
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);
    viewport->SetRenderPath(effectRenderPath);
    RENDERER->SetViewport(0, viewport);
}

void BlobCam::Update(float timeStep)
{
    Input* input{ GetSubsystem<Input>() };
    float moveSpeed{ timeStep * 420.0f * (0.5f + input->GetKeyDown(KEY_SHIFT)) };

    Quaternion spin{ 0.0f, 5.0f * input->GetMouseMoveX() * timeStep, 0.0f };
    node_->Rotate(spin, TS_WORLD);
    spin = Quaternion{ 5.0f * input->GetMouseMoveY() * timeStep, Vector3::RIGHT };
    node_->Rotate(spin, TS_LOCAL);

    Vector3 flatForward{ node_->GetWorldDirection().ProjectOntoPlane(Vector3::UP) };
    Vector3 flatRight{ node_->GetWorldRight().ProjectOntoPlane(Vector3::UP) };

    node_->Translate(moveSpeed * (flatRight * (input->GetKeyDown(KEY_D) - input->GetKeyDown(KEY_A)) +
                           Vector3::UP * (input->GetKeyDown(KEY_E) - input->GetKeyDown(KEY_Q)) +
                           flatForward * (input->GetKeyDown(KEY_W) - input->GetKeyDown(KEY_S))).Normalized(), TS_WORLD);
}
