
#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"
#include "witch.h"

namespace Dry {
class Node;
class Scene;
}

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }

    // Setup before engine initialization. Modifies the engine paramaters.
    void Setup() override;
    // Setup after engine initialization.
    void Start() override;
    // Cleanup after the main loop. Called by Application.
    void Stop() override;
    void Exit();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }

private:
    void CreateScene();
    Witch::Spell floorSheet_;
    bool drawDebug_;
    int terrainResolution_;
    Scene* scene_;
    Node* tickNode_;
//    void CreateTickmarks(const Vector<TriRune>& runes);
//    void CreateUpMarks(const Vector<TriRune>& runes);

    void CreateView();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void RenderDebug(StringHash eventType, VariantMap& eventData);
};

#endif // MASTERCONTROL_H
