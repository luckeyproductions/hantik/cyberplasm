# Bloblord

## Story

You're a bloblord.

## Objective

Rule the Realm.

## Structures

- Base
- Tentawall
- Spam (breeding ground)

## Units

- Flipworm
- Flypeswarm

## Items

- Donuts (2 two sizes, can carry 1)
- Horn (3 two sizes, can carry 2)
- Flypeggs
- Tentaseeds
