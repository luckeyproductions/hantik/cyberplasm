
#ifndef BLOBCAM_H
#define BLOBCAM_H

#include "luckey.h"


class BlobCam : public LogicComponent
{
    DRY_OBJECT(BlobCam, LogicComponent);

public:
    static void RegisterObject(Context* context);
    BlobCam(Context* context);

    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void CreateView();
};

#endif // BLOBCAM_H
