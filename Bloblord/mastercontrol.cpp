#include "blobcam.h"
#include "blob.h"
#include "block.h"
#include "rock.h"
#include "paper.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);


MasterControl::MasterControl(Context* context): Application(context),
    drawDebug_{ false },
    terrainResolution_{ 13 }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "bloblord.log";
    engineParameters_[EP_WINDOW_TITLE] = "Bloblord";
    engineParameters_[EP_WINDOW_ICON] = "blob.png";
//    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources";
}
void MasterControl::Start()
{
    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());


    context_->RegisterSubsystem(new Grimoire{ context_ });

    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_WRAP);
//    input->SetMouseVisible(true);

    Cyberplasm::RegisterObject(context_);
    Blob::RegisterObject(context_);
    Block::RegisterObject(context_);
    Rock::RegisterObject(context_);
    Paper::RegisterObject(context_);
    BlobCam::RegisterObject(context_);

    CreateScene();
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate));
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, RenderDebug));
}
void MasterControl::RenderDebug(StringHash eventType, VariantMap& eventData)
{
    if (GetSubsystem<Input>()->GetKeyPress(KEY_SPACE))
        drawDebug_ = !drawDebug_;

    if (!drawDebug_)
        return;

//    scene_->GetComponent<PhysicsWorld>()->DrawDebugGeometry(false);

    for (const Witch::Rune& r: floorSheet_)
    {
        scene_->GetComponent<DebugRenderer>()->AddCircle(r.Position() + r.Normal() * 0.1f, r.Normal() * M_LARGE_EPSILON, 1.0f, Color::YELLOW, 8);
        scene_->GetComponent<DebugRenderer>()->AddLine(r.Position(), r.Position() + r.Normal() * 5.0f, Color::RED);
    }
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    PhysicsWorld* phys{ scene_->CreateComponent<PhysicsWorld>() };
    phys->SetFps(17.0f);
    phys->SetGravity(Vector3::DOWN * 42.0f);
    scene_->CreateComponent<DebugRenderer>();
    Zone* zone{ RENDERER->GetDefaultZone() };
    zone->SetAmbientColor({ 0.65f, 0.8f, 0.85f });
    zone->SetFogStart(200.0f);
    zone->SetFogEnd(2000.0f);
    zone->SetFogColor(Color::WHITE);
    Skybox* sky{ scene_->CreateComponent<Skybox>() };
//    sky->SetModel();
    sky->SetModel(CACHE->GetResource<Model>("Models/Box.mdl"));
    sky->SetMaterial(CACHE->GetResource<Material>("Materials/Skybox.xml"));

    //Light
    {
        Node* lightNode{ scene_->CreateChild("Light") };
        lightNode->SetPosition(Vector3(5.0f, 42.0f, 23.0f) * 5.0f);
        lightNode->LookAt(Vector3::ZERO);
        Light* light{ lightNode->CreateComponent<Light>() };
        light->SetLightType(LIGHT_DIRECTIONAL);
        light->SetColor({ 0.98f, 1.0f, 0.9f });
        light->SetBrightness(1.17f);
        light->SetRange(1000.0f);
        //    light->SetFov(120.0f);
        light->SetCastShadows(true);
        light->SetDrawDistance(4000.0f);
        light->SetShadowDistance(4000.0f);
        light->SetShadowFadeDistance(2000.0f);
        light->SetShadowIntensity(0.2f);
        light->SetShadowResolution(1.0f);
        light->SetShadowBias({0.0075f, 0.001f, -0.05f});

    }
    //Floor light
    {
        Node* lightNode{ scene_->CreateChild("Light") };
        lightNode->SetPosition(Vector3::DOWN * 100.0f);
        lightNode->LookAt(Vector3::ZERO);
        Light* light{ lightNode->CreateComponent<Light>() };
        light->SetLightType(LIGHT_DIRECTIONAL);
        light->SetColor({ 0.7f, 1.0f, 0.6f });
        light->SetBrightness(0.23f);
        light->SetRange(1000.0f);
        light->SetDrawDistance(4000.0f);
    }

    //Floor
    Node* floorNode{ scene_->CreateChild("Floor") };
    RigidBody* floorRb{ floorNode->CreateComponent<RigidBody>() };
    floorRb->SetRestitution(0.3f);
    floorRb->SetFriction(0.3f);
    Cyberplasm* floor{ floorNode->CreateComponent<Cyberplasm>() };
    floor->SetMaterial(CACHE->GetResource<Material>("Materials/Floor.xml"));
    floor->SetCastShadows(true);

    float scale{ 64.0f };
    int n{ 23 };
    Witch::Spell scroll{};
    Vector3 basePos{};

    float y{};

    for (int f{ 0 }; f < n * n; ++f)
    {
        const bool edge{ f % n == 0 || f % n == n - 1 || f /n == n -1 || f / n == 0 };
        float y0{0.1f};
        if (scroll.Size() > n)
            y0 = scroll.At(f - n).Position().y_;
        float y{ Max(edge ? Random(2.0f, 5.0f) : 0.1f, RandomNormal(0.5f + (y + y0) * 0.0125f, (edge ? 1.0f : 0.25f))) };

        scroll.Push(Witch::Rune{scale * Vector3{ 0.5f * n - f / n + Random(-0.4f, 0.4f), (Random(2 - edge) ? Random(0.0f, 0.125f) : y), 0.5f * n - 1.5f * (f % n) + Random(-0.4f, 0.4f) }, {Random(-.5f, .5f), RandomNormal(3.0f, 0.25f), Random(-.5f, .5f)}});

        if (scroll.At(f).Position().Length() < scale * n / 4 && scroll.At(f).Position().y_ > basePos.y_ && scroll.At(f).Position().y_ < y && scroll.At(f).Normal().y_ > 0.95f)
            basePos = scroll.At(f).Position() + Vector3::UP * 5.0f;
    }

    floorSheet_ = scroll;
    floor->Generate(Grimoire::Sheet(scroll, n), 2);
    floorNode->CreateComponent<CollisionShape>()->SetTriangleMesh(floor->GetModel());
    floor->Generate(Grimoire::Sheet(scroll, n), terrainResolution_);

    //Blob
    for (int b { 0 }; b < 1; ++b)
    {
        Node* blob{ scene_->CreateChild("Blob") };
        blob->CreateComponent<Blob>();
        blob->SetPosition(basePos + Vector3::UP * 13.0f);
    }

    //Camera
    scene_->CreateChild("Camera")->CreateComponent<BlobCam>();

    Node* baseNode{ scene_->CreateChild("Base") };
    baseNode->SetPosition(basePos);
    baseNode->SetScale(0.25f);
    //Tori
    for (int t{ 0 }; t < 2; ++t)
    {
        Node* torusNode{ baseNode->CreateChild("Torus") };
        torusNode->CreateComponent<RigidBody>()->SetRestitution(0.2f);
        Cyberplasm* torus{ torusNode->CreateComponent<Cyberplasm>() };
        torus->SetMaterial(CACHE->GetResource<Material>("Materials/Rock.xml"));
        torusNode->CreateComponent<CollisionShape>()->SetTriangleMesh(Grimoire::Summon(Geonomicon::Torus(10.0f + 30.0f * t, 20.0f + 40.0f * t), t));
        torus->Generate(Geonomicon::Torus(10.0f + 30.0f * t, 20.0f + 40.0f * t), 2 + t);
    }

    for (int c{ 0 }; c < 3; ++c)
    {
//        Witch::Spell path{{Vector3::DOWN * 23.0f}};
        unsigned s{ 42 };
        Vector3 area{ 50.0f, 100.0f, 50.0f };
//        for (int p{ 1 }; p < (s + 1); ++p)
//        {
//            path.Push({{-0.5f * area.x_ + Random(area.x_),
//                        p * area.y_ / s,
//                        -0.5f * area.z_ + Random(area.z_)}});
//        }
        Witch::Spell pattern{};
        for (int p{ 1 }; p <= s; ++p)
        {
            Witch::Spell ellipse{ Geonomicon::Ellipse({ 2.0f * sqrt(p + 1), 3.0f * sqrt(p + 1) }, 4u) };

            if (p%2)
                for (Witch::Rune& r: ellipse)
                    r = Witch::Rune{ r.Position() * (0.5f + 0.001f * p * p), Quaternion{ 45.0f / (p*p), r.Normal().CrossProduct(Vector3::FORWARD) } * r.Normal() };

            pattern.Push(ellipse);
        }
        Vector3 offset{ Vector3::RIGHT * 34.0f };

        Witch::Rune runeA{ Vector3::UP * area.y_ + offset, Vector3::LEFT };
        Witch::Rune runeB{ Vector3::DOWN + offset };
        runeA = runeA.Transformed(Matrix3x4{Quaternion{120.0f * c, Vector3::UP}.RotationMatrix()});
        runeB = runeB.Transformed(Matrix3x4{Quaternion{120.0f * c, Vector3::UP}.RotationMatrix()});
        Witch::Wand pathWand{ runeA, runeB };
        Witch::Spell path{ pathWand.Plot(s) };

        const Witch::Rune& firstRune{ path.Front() };
        const Witch::Rune& lastRune{  path.Back() };

        const Vector3 dIn{ Vector3::UP };
        const Vector3 dOut{ (runeB.Position() - path.At(path.Size() - 2).Position()).Normalized() };

        Witch::Rune tip{   runeA.Position() + dIn  * 2.0f, dIn };
        Witch::Rune stump{ runeB.Position() + dOut * 3.0f, dOut };

        Node* callis{ baseNode->CreateChild("Callis") };
        callis->CreateComponent<Cyberplasm>()->Generate(Geonomicon::Callis(path,{Grimoire::Rings(pattern, 4u)},tip, stump), 4u);
        callis->GetComponent<Cyberplasm>()->SetMaterial(CACHE->GetResource<Material>("Materials/Rock.xml"));
        callis->GetComponent<Cyberplasm>()->SetCastShadows(true);
    }
}

void MasterControl::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    Input* input{ GetSubsystem<Input>() };

    if (input->GetKeyPress(KEY_KP_PLUS) || input->GetKeyPress(KEY_KP_MINUS))
    {
        terrainResolution_ = Clamp(terrainResolution_ + input->GetKeyPress(KEY_KP_PLUS) - input->GetKeyPress(KEY_KP_MINUS), 0, 128);
        scene_->GetChild("Floor")->GetComponent<Cyberplasm>()->Generate(Grimoire::Sheet(floorSheet_, 23), terrainResolution_);
    }

    //Rock
    PODVector<Node*> rocks{};
    scene_->GetChildrenWithComponent<Rock>(rocks);
    PODVector<Node*> blocks{};
    scene_->GetChildrenWithComponent<Block>(rocks);

//    return;/////////////////////////////////////////////

    if (rocks.Size() + blocks.Size() < 23)
    {
        Node* rock{ scene_->CreateChild("Rock") };
        rock->SetPosition((Vector3::UP * Pow(Random(2.0f, 3.0f), 2.0f) + Quaternion{ Random(360.0f), Vector3::UP } * Vector3::FORWARD * RandomNormal(0.0f, 1.0f)) * 17.0f);

        if (Random(5))
            rock->CreateComponent<Rock>();
        else
            rock->CreateComponent<Block>()->SetSize({ Random(3.0f, 30.0f), Random(3.0f, 15.0f), Random(3.0f, 15.0f) }, Random(1.0f, 5.0f));
    }
    //Paper
    PODVector<Node*> papers{};
    scene_->GetChildrenWithComponent<Paper>(papers);

    if (papers.Size() < 42)
    {
        for (int p{ 0 }; p < 1; ++p)
        {
            Node* paper{ scene_->CreateChild("Paper") };
            paper->SetPosition((Vector3::UP * Random(0.0f, 3.0f) + Quaternion{ Random(360.0f), Vector3::UP } * Vector3::FORWARD * RandomNormal(0.0f, 1.0f)) * 5.0f);
            paper->CreateComponent<Paper>();
        }
    }
}
