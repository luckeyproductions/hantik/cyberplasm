#ifndef BLOB_H
#define BLOB_H

#include "cyberplasm.h"

class Blob: public LogicComponent
{
    DRY_OBJECT(Blob, LogicComponent);

public:
    Blob(Context* context);
    static void RegisterObject(Context* context);

    void PostUpdate(float timeStep) override;
    void Start() override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void PrepareAnim();
    void RandomizeOffset(int i);

    Cyberplasm* cyberplasm_;
    Cyberplasm* boundingPlasm_;
    float speed_;
    Vector<float> blobTime_;
    Vector<Witch::Rune> refRunes_;
    Vector<Witch::Rune> offRunes_;
    Vector<Witch::Rune> onRunes_;
};

#endif // BLOB_H
