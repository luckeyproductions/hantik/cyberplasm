#include "block.h"
#include "rock.h"
#include "paper.h"
#include "clerk.h"
#include "potato.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);


MasterControl::MasterControl(Context* context): Application(context)
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "klur.log";
    engineParameters_[EP_WINDOW_TITLE] = "Klur";
    engineParameters_[EP_WINDOW_ICON] = "klur.png";
//    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources";
}
void MasterControl::Start()
{
    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());


    context_->RegisterSubsystem(new Grimoire{ context_ });

    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_WRAP);
//    input->SetMouseVisible(true);

    Cyberplasm::RegisterObject(context_);
    Block::RegisterObject(context_);
    Rock::RegisterObject(context_);
    Paper::RegisterObject(context_);
    Clerk::RegisterObject(context_);
    Potato::RegisterObject(context_);

    CreateScene();
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate));
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, RenderDebug));
}
void MasterControl::RenderDebug(StringHash eventType, VariantMap& eventData)
{
//    scene_->GetComponent<PhysicsWorld>()->DrawDebugGeometry(false);
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<Clerk>();

    PhysicsWorld* phys{ scene_->CreateComponent<PhysicsWorld>() };
    phys->SetFps(17.0f);
    phys->SetGravity(Vector3::DOWN * 42.0f);
    scene_->CreateComponent<DebugRenderer>();
    Zone* zone{ RENDERER->GetDefaultZone() };
    zone->SetAmbientColor({ 0.34f, 0.4f, 0.42f });
    zone->SetFogStart(500.0f);
    zone->SetFogEnd(1000.0f);

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(5.0f, 42.0f, 23.0f) * 5.0f);
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetColor({ 0.98f, 1.0f, 0.9f });
    light->SetRange(420.0f);
//    light->SetFov(120.0f);
    light->SetCastShadows(true);
    light->SetShadowDistance(420.0f);
    light->SetShadowIntensity(0.3f);
    light->SetShadowResolution(1.0f);


    //Potato
    for (int b { 0 }; b < 1; ++b)
    {
        Node* potato{ scene_->CreateChild("Blob") };
        potato->CreateComponent<Potato>();
    }
/*
    //Floor
    Node* floorNode{ scene_->CreateChild("Floor") };
    //    StaticModel* floor{ floorNode->CreateComponent<StaticModel>() };
    Cyberplasm* floor{ floorNode->CreateComponent<Cyberplasm>() };
    floor->SetMaterial(CACHE->GetResource<Material>("Materials/Floor.xml"));
    floor->SetCastShadows(true);

    float scale{ 55.0f };
    int n{ 3 };
    Witch::Spell scroll{};

    for (int f{ 0 }; f < n * n; ++f)
    {
        float fx{ 2.0f * (f % n - 1) };
        float fz{ 2.0f * (f / n - 1) };
        Matrix3x4 shift{};
        shift.SetTranslation({ fx, 0.0f, fz });
        scroll.Push({
            Witch::Rune{ { -1.0f, 0.0f,  1.0f }, { 0.25f, 1.0f, 0.5f } }.Transformed(shift),
            Witch::Rune{ {  1.0f, 0.0f,  1.0f }, { 0.25f, 1.0f, 0.5f } }.Transformed(shift),
            Witch::Rune{ { -1.0f, 0.0f, -1.0f }, { 0.25f, 1.0f, 0.5f } }.Transformed(shift),
            Witch::Rune{ {  1.0f, 0.0f, -1.0f }, { 0.25f, 1.0f, 0.5f } }.Transformed(shift)
        });
    }

    floor->Generate(Grimoire::Quads(scroll), 9);

    floorNode->CreateComponent<RigidBody>()->SetRestitution(0.3f);
    floorNode->CreateComponent<CollisionShape>()->SetBox(n * Vector3::ONE * 2.0f, n * Vector3::DOWN * 1.0f); //SetTriangleMesh(floor->GetModel())
    floorNode->SetScale(scale);
    floorNode->SetPosition(Vector3::DOWN * 2.3f);
*/
    CreateView();
}


void MasterControl::CreateView()
{
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE * 100.0f);
    cameraNode->LookAt(Vector3::UP * 10.0f);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetFov(80);
    camera->SetFarClip(1000.0f);

    Viewport* viewport{  new Viewport(context_, scene_, camera) };
    RenderPath* effectRenderPath{viewport->GetRenderPath()};
    effectRenderPath->Load(GetSubsystem<ResourceCache>()->GetResource<XMLFile>("RenderPaths/Forward.xml"));
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", 0.23f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2(1.f, 0.17f));
    effectRenderPath->SetEnabled("BloomHDR", true);
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);
    viewport->SetRenderPath(effectRenderPath);
    RENDERER->SetViewport(0, viewport);
}

void MasterControl::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    Input* input{ GetSubsystem<Input>() };
    float t{ eventData[Update::P_TIMESTEP].GetFloat() };
    float moveSpeed{ t * 100.0f * (0.5f + input->GetKeyDown(KEY_SHIFT)) };


    Node* camNode{ scene_->GetChild("Camera") };
    Quaternion spin{ 0.0f, 5.0f * input->GetMouseMoveX() * t, 0.0f };
    camNode->Rotate(spin, TS_WORLD);
    spin = Quaternion{ 5.0f * input->GetMouseMoveY() * t, Vector3::RIGHT };
    camNode->Rotate(spin, TS_LOCAL);

    camNode->Translate(moveSpeed * Vector3{
                           input->GetKeyDown(KEY_D) - input->GetKeyDown(KEY_A),
                           0.0f,
                           input->GetKeyDown(KEY_W) - input->GetKeyDown(KEY_S)}, TS_LOCAL);
    camNode->Translate(moveSpeed * Vector3{
                           0.0f,
                           input->GetKeyDown(KEY_E) - input->GetKeyDown(KEY_Q),
                           0.0f}, TS_WORLD);
}
