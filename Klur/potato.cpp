
#include "potato.h"

using namespace Witch;

void Potato::RegisterObject(Context* context)
{
    context->RegisterFactory<Potato>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

Potato::Potato(Context* context): LogicComponent(context),
    cyberplasm_{ nullptr },
    speed_{ 0.55f },
    potatoTime_{},
    refRunes_{},
    offRunes_{},
    onRunes_{}
{
    PrepareAnim();
}

void Potato::Start()
{
}

void Potato::OnNodeSet(Node *node)
{
    if (!node)
        return;

    mess_ = node_->CreateChild("Mess");
    cyberplasm_ = node_->CreateComponent<Cyberplasm>();
    cyberplasm_->SetCastShadows(true);
    cyberplasm_->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Rock.xml"));
    node_->SetScale(2.3f);
}

void Potato::PostUpdate(float timeStep)
{
    if (speed_ == 0.0f)
        return;

    speed_ = 0.0f;

    Input* input{ GetSubsystem<Input>() };
/*
    speed_ += Sqrt(speed_) * input->GetMouseMoveWheel() * 0.1f;
    if (speed_ < M_LARGE_EPSILON)
        speed_ = M_LARGE_EPSILON;

    for (int b{ 0 }; b < potatoTime_.Size(); ++b)
    {
        potatoTime_[b] += speed_ * (0.75f - 0.25f * sin(b + GetScene()->GetElapsedTime() * M_TAU * 0.001f)) * timeStep;

        if (potatoTime_[b] > 1.0f)
        {
            RandomizeOffset(b);
            potatoTime_[b] = 0.0f;
        }
    }
*/
    Spell spell{};

    Vector<Rune> current{};
    float d{ 5.0f};

    for (int r{ 0 }; r < refRunes_.Size(); ++r)
    {

        Vector3 refPos{  refRunes_.At(r).Position() };
        Vector3 onPos{    onRunes_.At(r).Position() };
        Vector3 offPos{  offRunes_.At(r).Position() };
        Vector3 refNorm{ refRunes_.At(r).Normal() };
        Vector3 onNorm{   onRunes_.At(r).Normal() };
        Vector3 offNorm{ offRunes_.At(r).Normal() };

        onNorm += refNorm;
        offNorm += refNorm;

        Vector3 pos{ Lerp(refPos + onPos * d, refPos + offPos * d, -0.5f * cos(M_PI * potatoTime_.At(r)) + 0.5f) };
        Vector3 norm{ Lerp(refNorm + onNorm, refNorm + offNorm, -0.5f * cos(M_PI * potatoTime_.At(r)) + 0.5f) };
        current.Push(Rune{ pos, norm });
    }

    int n{ refRunes_.Size() - 2 };

    for (int i{ 0 }; i < n; ++i)
    {
        Spell triRune{ current.Front(),
                         current.At(1 + (i % n)),
                         current.At(1 + ((i + 1) % n)) };
        spell.Push(triRune);

        Spell triRuneM{ current.Back(),
                          triRune.At(2),
                          triRune.At(1) };
        spell.Push(triRuneM);
    }
//    CreateTickmarks(sect);
//    CreateUpMarks(runes);
    cyberplasm_->Generate(Grimoire::Tris(spell), 12);
}

void Potato::PrepareAnim()
{
    const int n{ 5 };
    const float a{ 360.0f / n };
    const float r{ 4.20f };

    refRunes_.Push(Rune{ { 0.0f, 1.75f * r, 0.0f }, Vector3::UP });

    for (int i{ 0 }; i < n; ++i)
    {
        Vector3 pos{ Sin(a * i) * r,  Random(-0.125f, 0.125f) * r, Cos(a * i) * r };
        Vector3 norm{ pos.ProjectOntoPlane(Vector3::UP).Normalized() };

        refRunes_.Push(Rune{ pos, norm });
    }

    refRunes_.Push(Rune{ { 0.0f, -1.75f * r, 0.0f }, Vector3::DOWN });

    for (int o{ 0 }; o < refRunes_.Size(); ++o)
    {
        offRunes_.Push(Rune{ Vector3::ZERO, refRunes_.At(o).Normal() });
        onRunes_.Push(offRunes_[o]);
        potatoTime_.Push(Random(1.0f));

        RandomizeOffset(o);
    }
}

void Potato::RandomizeOffset(int i)
{
    assert(i >= 0 && i < offRunes_.Size());

    Vector3 newOffset{ Vector3{ Random(-0.25f, 0.25f),
                    Random(-0.5f, 0.5f),
                    Random(-0.25f, 0.25f) } };

    onRunes_[i] = offRunes_[i];
    offRunes_[i] = Rune{ newOffset, newOffset };
}
