#ifndef POTATO_H
#define POTATO_H

#include "cyberplasm.h"

class Potato: public LogicComponent
{
    DRY_OBJECT(Potato, LogicComponent);

public:
    Potato(Context* context);
    static void RegisterObject(Context* context);

    void PostUpdate(float timeStep) override;
    void Start() override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void PrepareAnim();
    void RandomizeOffset(int i);

    Node* mess_;
    Cyberplasm* cyberplasm_;
    float speed_;
    Vector<float> potatoTime_;
    Vector<Witch::Rune> refRunes_;
    Vector<Witch::Rune> offRunes_;
    Vector<Witch::Rune> onRunes_;
};

#endif // POTATO_H
