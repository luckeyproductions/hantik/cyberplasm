HEADERS += \
    Lavaworld/lavacam.h \
    Lavaworld/lava.h \
    Lavaworld/mastercontrol.h

SOURCES += \
    Lavaworld/lavacam.cpp \
    Lavaworld/lava.cpp \
    Lavaworld/mastercontrol.cpp
