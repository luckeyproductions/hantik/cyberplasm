
#include "lavacam.h"

void LavaCam::RegisterObject(Context* context)
{
    context->RegisterFactory<LavaCam>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

LavaCam::LavaCam(Context* context) : LogicComponent(context)
{
}

void LavaCam::OnNodeSet(Node* node)
{
    if (node)
        CreateView();
}


void LavaCam::CreateView()
{
    node_->SetPosition(Vector3::DOWN * 1.5f * 256.0f * M_1_SQRT3);
//    node_->LookAt(node_->GetPos);
    Camera* camera{ node_->CreateComponent<Camera>() };
    camera->SetFov(90);
    camera->SetFarClip(2000.0f);

    Viewport* viewport{  new Viewport(context_, GetScene(), camera) };
    RenderPath* effectRenderPath{viewport->GetRenderPath()};
    effectRenderPath->Load(GetSubsystem<ResourceCache>()->GetResource<XMLFile>("RenderPaths/Forward.xml"));
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", 0.23f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2(0.9f, 0.1f));
    effectRenderPath->SetEnabled("BloomHDR", true);
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);
    viewport->SetRenderPath(effectRenderPath);
    RENDERER->SetViewport(0, viewport);
}

void LavaCam::Update(float timeStep)
{
    Input* input{ GetSubsystem<Input>() };
    float moveSpeed{ timeStep * 420.0f * (0.5f + input->GetKeyDown(KEY_SHIFT)) };

    Quaternion spin{ 5.0f * input->GetMouseMoveX() * timeStep, node_->GetUp() };
    node_->Rotate(spin, TS_WORLD);
    spin = Quaternion{ 5.0f * input->GetMouseMoveY() * timeStep, Vector3::RIGHT };
    node_->Rotate(spin, TS_LOCAL);

    Vector3 flatForward{ node_->GetWorldDirection().ProjectOntoPlane(node_->GetPosition()) };
    Vector3 flatRight{ node_->GetWorldRight().ProjectOntoPlane(node_->GetPosition()) };

    node_->Translate(moveSpeed * (flatRight * (input->GetKeyDown(KEY_D) - input->GetKeyDown(KEY_A)) +
                           node_->GetPosition().Normalized() * (input->GetKeyDown(KEY_E) - input->GetKeyDown(KEY_Q)) +
                           flatForward * (input->GetKeyDown(KEY_W) - input->GetKeyDown(KEY_S))).Normalized(), TS_WORLD);

    node_->SetPosition(node_->GetPosition().Normalized() * 1.5f * 256.0f * M_1_SQRT3);
    node_->LookAt((node_->GetPosition().Normalized() + node_->GetDirection()).Normalized() * 1.5f * 256.0f * M_1_SQRT3, -node_->GetPosition());
}
