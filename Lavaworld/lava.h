#ifndef LAVA_H
#define LAVA_H

#include "cyberplasm.h"

class Craft: public LogicComponent
{
    DRY_OBJECT(Craft, LogicComponent);

public:
    Craft(Context* context);
    static void RegisterObject(Context* context);

    void PostUpdate(float timeStep) override;
    void Start() override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void PrepareAnim();
    void RandomizeOffset(int i);

    Cyberplasm* cyberplasm_;
    Cyberplasm* boundingPlasm_;
    float speed_;
    Vector<float> lavaTime_;
    Vector<Witch::Rune> refRunes_;
    Vector<Witch::Rune> offRunes_;
    Vector<Witch::Rune> onRunes_;
};

#endif // LAVA_H
