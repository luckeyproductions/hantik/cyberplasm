
#ifndef LAVACAM_H
#define LAVACAM_H

#include "luckey.h"


class LavaCam : public LogicComponent
{
    DRY_OBJECT(LavaCam, LogicComponent);

public:
    static void RegisterObject(Context* context);
    LavaCam(Context* context);

    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void CreateView();
};

#endif // LAVACAM_H
