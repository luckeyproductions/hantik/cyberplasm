#ifndef CRAFT_H
#define CRAFT_H

#include "cyberplasm.h"

enum BrushShape{ SPHERE, CUBE };

class Craft: public LogicComponent
{
    DRY_OBJECT(Craft, LogicComponent);

public:
    Craft(Context* context);
    static void RegisterObject(Context* context);

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;

    bool IsSolid(const IntVector3& coords) const { return blocks_.Keys().Contains(coords); }

protected:
    void OnNodeSet(Node* node) override;

private:
    bool requireUpdate_{};
    IntVector3 cursorCoords_;
    unsigned char brushSize_;
    BrushShape brushShape_;

    HashMap<IntVector3, Node*> blocks_;
    HashSet<IntVector3> requiresUpdate_;

    Vector<PODVector<Vector3> > memorized_;
    HashMap<unsigned, SharedPtr<Geometry> > forecast_;

    void RenderDebug(StringHash eventType, VariantMap& eventData);
    Vector<IntVector3> Brush() const;
};

#endif // CRAFT_H
