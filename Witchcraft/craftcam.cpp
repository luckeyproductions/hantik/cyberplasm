#include "craftcam.h"

void CraftCam::RegisterObject(Context* context)
{
    context->RegisterFactory<CraftCam>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

CraftCam::CraftCam(Context* context): LogicComponent(context),
    walk_{ false }
{
}

void CraftCam::OnNodeSet(Node* node)
{
    if (node)
        CreateView();
}


void CraftCam::CreateView()
{
    node_->SetPosition({ 0.0f, 2.0f, 3.0f });
    node_->LookAt(Vector3::ZERO);
    Camera* camera{ node_->CreateComponent<Camera>() };
    camera->SetFov(90);
    camera->SetNearClip(M_MIN_NEARCLIP);
    camera->SetFarClip(420.0f);

    Viewport* viewport{  new Viewport(context_, GetScene(), camera) };
    RenderPath* effectRenderPath{viewport->GetRenderPath()};
    effectRenderPath->Load(GetSubsystem<ResourceCache>()->GetResource<XMLFile>("RenderPaths/Forward.xml"));
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", 0.42f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2{ 0.9f, 0.17f });
    effectRenderPath->SetEnabled("BloomHDR", true);
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);
    viewport->SetRenderPath(effectRenderPath);
    RENDERER->SetViewport(0, viewport);
}

void CraftCam::Update(float timeStep)
{
    Input* input{ GetSubsystem<Input>() };

    Quaternion spin{ 0.0f, 5.0f * input->GetMouseMoveX() * timeStep, 0.0f };
    node_->Rotate(spin, TS_WORLD);
    spin = Quaternion{ 5.0f * input->GetMouseMoveY() * timeStep, Vector3::RIGHT };
    node_->Rotate(spin, TS_LOCAL);

    const Vector3 flatForward{ node_->GetWorldDirection().ProjectOntoPlane(Vector3::UP) };
    const Vector3 flatRight{ node_->GetWorldRight().ProjectOntoPlane(Vector3::UP) };

    if (!walk_)
    {
        const float moveSpeed{ timeStep * 42.0f * (0.5f + input->GetKeyDown(KEY_SHIFT)) };

        node_->Translate(moveSpeed *
                         (flatRight   * (input->GetKeyDown(KEY_D) - input->GetKeyDown(KEY_A)) +
                          Vector3::UP * (input->GetKeyDown(KEY_E) - input->GetKeyDown(KEY_Q)) +
                          flatForward * (input->GetKeyDown(KEY_W) - input->GetKeyDown(KEY_S))).Normalized(), TS_WORLD);
    }
    else
    {
        const float moveSpeed{ timeStep * 2.3f * (0.5f + input->GetKeyDown(KEY_SHIFT)) };

        Vector3 move{ moveSpeed *
                    (flatRight   * (input->GetKeyDown(KEY_D) - input->GetKeyDown(KEY_A)) +
                     flatForward * (input->GetKeyDown(KEY_W) - input->GetKeyDown(KEY_S))).Normalized() };


        Vector3 target{ node_->GetWorldPosition() + move };

//        move = move.Normalized() * Clamp(move.Length(), 0.0f, Max(0.0f, (node_->GetWorldPosition() - r.position_).Length() - 0.5f));

        const Ray ray{ node_->GetWorldPosition() + move, Vector3::DOWN };
        PODVector<RayQueryResult> result{};
        RayOctreeQuery query{ result, ray, RAY_TRIANGLE, 1.0f };
        GetScene()->GetComponent<Octree>()->RaycastSingle(query);

        if (result.Size())
        {
            RayQueryResult r{ result.Front() };
//            move = r.position_ + Vector3::UP * 0.375f;
        }

        node_->Translate(move, TS_WORLD);
    }
}

void CraftCam::StartWalk(const IntVector3& coords)
{
    node_->SetPosition(Vector3{ coords } + Vector3::UP * 0.125f);
    walk_ = true;
}
