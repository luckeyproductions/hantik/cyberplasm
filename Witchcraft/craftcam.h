
#ifndef CRAFTCAM_H
#define CRAFTCAM_H

#include "luckey.h"


class CraftCam : public LogicComponent
{
    DRY_OBJECT(CraftCam, LogicComponent);

public:
    static void RegisterObject(Context* context);
    CraftCam(Context* context);

    void Update(float timeStep) override;
    void StartWalk(const IntVector3& coords);
    void EndWalk()   { walk_ = false; }
    bool IsWalking() const { return  walk_; }

protected:
    void OnNodeSet(Node* node) override;

private:
    void CreateView();

    bool walk_;
};

#endif // CRAFTCAM_H
