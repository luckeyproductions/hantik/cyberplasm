#include "craftcam.h"
#include "craft.h"
#include "block.h"
#include "rock.h"
#include "paper.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);


MasterControl::MasterControl(Context* context): Application(context)
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "witchcraft.log";
    engineParameters_[EP_WINDOW_TITLE] = "Witchcraft";
    engineParameters_[EP_WINDOW_ICON] = "witchcraft.png";
//    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources";
}
void MasterControl::Start()
{
    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    context_->RegisterSubsystem(new Grimoire{ context_ });

    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_WRAP);
//    input->SetMouseVisible(true);

    Cyberplasm::RegisterObject(context_);
    Craft::RegisterObject(context_);
    Block::RegisterObject(context_);
    Rock::RegisterObject(context_);
    Paper::RegisterObject(context_);
    CraftCam::RegisterObject(context_);

    CreateScene();
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, RenderDebug));
}
void MasterControl::RenderDebug(StringHash eventType, VariantMap& eventData)
{
    scene_->GetComponent<PhysicsWorld>()->DrawDebugGeometry(false);
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    PhysicsWorld* phys{ scene_->CreateComponent<PhysicsWorld>() };
    phys->SetFps(17.0f);
    phys->SetGravity(Vector3::DOWN * 42.0f);
    scene_->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);
    Zone* zone{ RENDERER->GetDefaultZone() };
    zone->SetAmbientColor({ 0.65f, 0.8f, 0.85f });
    zone->SetFogStart(100.0f);
    zone->SetFogEnd(420.0f);
    zone->SetFogColor(Color::RED * 0.2f);

    //Sky
    Skybox* sky{ scene_->CreateComponent<Skybox>() };
    sky->SetModel(CACHE->GetResource<Model>("Models/Box.mdl"));
    sky->SetMaterial(CACHE->GetResource<Material>("Materials/Skybox.xml"));

    //Light
    {
        Node* lightNode{ scene_->CreateChild("Light") };
        lightNode->SetPosition(Vector3(5.0f, 42.0f, 23.0f) * 5.0f);
        lightNode->LookAt(Vector3::ZERO);
        Light* light{ lightNode->CreateComponent<Light>() };
        light->SetLightType(LIGHT_DIRECTIONAL);
        light->SetColor({ 0.98f, 1.0f, 0.9f });
        light->SetBrightness(1.17f);
        light->SetRange(1000.0f);
        //    light->SetFov(120.0f);
        light->SetCastShadows(true);
        light->SetDrawDistance(4000.0f);
        light->SetShadowDistance(4000.0f);
        light->SetShadowFadeDistance(2000.0f);
        light->SetShadowIntensity(0.2f);
        light->SetShadowResolution(1.0f);
//        light->SetShadowBias({0.0075f, 0.001f, -0.05f});

    }
    //Floor light
    {
        Node* lightNode{ scene_->CreateChild("Light") };
        lightNode->SetPosition(Vector3::DOWN * 100.0f);
        lightNode->LookAt(Vector3::ZERO);
        Light* light{ lightNode->CreateComponent<Light>() };
        light->SetLightType(LIGHT_DIRECTIONAL);
        light->SetColor({ 0.7f, 1.0f, 0.6f });
        light->SetBrightness(0.23f);
        light->SetRange(1000.0f);
        light->SetDrawDistance(4000.0f);
    }

    //Craft
    Node* craft{ scene_->CreateChild("Craft") };
    craft->CreateComponent<Craft>();

    //Camera
    scene_->CreateChild("Camera")->CreateComponent<CraftCam>();
}
