#include "craftcam.h"
#include "geonomicon.h"
#include "craft.h"

using namespace Witch;

void Craft::RegisterObject(Context* context)
{
    context->RegisterFactory<Craft>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

Craft::Craft(Context* context): LogicComponent(context),
    blocks_{},
    cursorCoords_{},
    brushSize_{ 1 },
    brushShape_{ SPHERE }
{
}

void Craft::OnNodeSet(Node *node)
{
    if (!node)
        return;

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Craft, RenderDebug));
}

void Craft::RenderDebug(StringHash eventType, VariantMap& eventData)
{
    //Cursor
    for (const IntVector3& brushCoords: Brush())
    {
        GetScene()->GetComponent<DebugRenderer>()->AddBoundingBox(
                    BoundingBox{{ brushCoords - Vector3::ONE * 0.5f}, { brushCoords + Vector3::ONE * 0.5f}},
                    Color::GREEN, false);

        for (int xO{ 0 }; xO < 2; ++xO)
        for (int yO{ 0 }; yO < 2; ++yO)
        for (int zO{ 0 }; zO < 2; ++zO)
        {
            const IntVector3 offPoint{ xO, yO, zO };

            GetScene()->GetComponent<DebugRenderer>()->AddCross(
            { brushCoords - Vector3{ 0.5f - xO, 0.5f - yO, 0.5f - zO, }}, 0.25f,
                        Color::YELLOW, true);
        }
    }
}

Vector<IntVector3> Craft::Brush() const
{
    Vector<IntVector3> brush;

    for (int xO{ -brushSize_ }; xO <= (brushSize_); ++xO)
    for (int yO{ -brushSize_ }; yO <= (brushSize_); ++yO)
    for (int zO{ -brushSize_ }; zO <= (brushSize_); ++zO)
    {
        const IntVector3 offset{ IntVector3{ xO, yO, zO } };

        if (brushShape_ == SPHERE && Vector3{ offset }.DistanceToPoint(Vector3::ZERO) > brushSize_)
            continue;

        const IntVector3 brushCoords{ offset + cursorCoords_};

        brush.Push(brushCoords);
    }

    return brush;
}

void Craft::Update(float timeStep)
{
    Input* input{ GetSubsystem<Input>() };
    cursorCoords_.y_ += input->GetMouseMoveWheel();

    if (input->GetKeyPress(KEY_KP_PLUS))
    {
        if (brushSize_ < 3)
            ++brushSize_;
    }
    if (input->GetKeyPress(KEY_KP_MINUS))
    {
        if (brushSize_ > 0)
            --brushSize_;
    }

    if (input->GetKeyPress(KEY_KP_MULTIPLY))
    {
        if (brushShape_ == SPHERE)
            brushShape_ = CUBE;
        else
            brushShape_ = SPHERE;
    }

    Node* camNode{ GetScene()->GetChild("Camera") };

    if (input->GetKeyPress(KEY_SPACE))
    {
        CraftCam* cam{ camNode->GetComponent<CraftCam>() };

        if (!cam->IsWalking())
        {
            IntVector3 walkCoords{ cursorCoords_ };

            while (blocks_.Keys().Contains(walkCoords))
                walkCoords += IntVector3::UP;

            cam->StartWalk(walkCoords);

            return;
        }
        else
        {
            cam->EndWalk();
        }
    }

    Plane plane{ Vector3::UP, Vector3::UP * cursorCoords_.y_ };
    const Ray ray{ camNode->GetWorldPosition(), camNode->GetDirection() };
    float hitDist{ ray.HitDistance(plane)};

    if (hitDist != M_INFINITY)
    {
        const IntVector3& newCoords{ VectorRoundToInt(camNode->GetPosition() + camNode->GetDirection() * hitDist) };

        if (cursorCoords_ != newCoords || input->GetMouseButtonPress(MOUSEB_ANY))
        {
            cursorCoords_ = newCoords;

            if (input->GetMouseButtonDown(MOUSEB_LEFT))
            {
                for (const IntVector3& brushCoords: Brush())
                {
                    if (!blocks_.Contains(brushCoords))
                    {
                        Node* blockNode{ node_->CreateChild("Block") };
                        blockNode->SetPosition(Vector3{ brushCoords });
                        blockNode->CreateComponent<StaticModel>()->SetCastShadows(true);

                        blocks_.Insert(Pair<IntVector3, Node*>{ brushCoords, blockNode });
                        requiresUpdate_.Insert(brushCoords);
                    }
                }
            }
            else if (input->GetMouseButtonDown(MOUSEB_RIGHT))
            {
                for (const IntVector3& brushCoords: Brush())
                {
                    if (blocks_.Contains(brushCoords))
                    {
                        blocks_[brushCoords]->Remove();
                        blocks_.Erase(brushCoords);
                        requiresUpdate_.Insert(brushCoords);
                    }
                }
            }
        }
    }
}

void Craft::PostUpdate(float timeStep)
{
    const HashSet<IntVector3> requiredUpdate{ requiresUpdate_ };

    //Add occupied neighbuors, skip removed
    for (const IntVector3& coords: requiredUpdate)
    {
        for (int xO{ -1 }; xO <= 1; ++xO)
        for (int yO{ -1 }; yO <= 1; ++yO)
        for (int zO{ -1 }; zO <= 1; ++zO)
        {
            if (xO == 0 && yO == 0 && zO == 0)
            {
                if (!blocks_.Keys().Contains(coords))
                    requiresUpdate_.Erase(coords);

                continue;
            }

            const IntVector3 offCoords{ IntVector3{ xO, yO, zO } + coords};

            if (blocks_.Keys().Contains(offCoords))
            {
                requiresUpdate_.Insert(offCoords);
            }
        }
    }

    //Update block models
    for (const IntVector3& coords: requiresUpdate_)
    {
        StaticModel* blockModel{ blocks_[coords]->GetComponent<StaticModel>() };


        Sect sect{};

        Polyhedron positions{ BoundingBox{ -Vector3::ONE * 0.5f, Vector3::ONE * 0.5f } };
        Polyhedron normals{ positions };

        for (unsigned f{ 0 }; f < positions.faces_.Size(); ++f)
        {
            unsigned numVerts{ positions.faces_.At(f).Size() };

            const Vector<Vector3> face{ positions.faces_.At(f).Buffer(), numVerts };
            IntVector3 faceNormal{ VectorRoundToInt(Sum(face.Begin(), face.End()).Normalized()) };
            if (blocks_.Keys().Contains(coords + faceNormal))
            {
                positions.faces_.At(f).Clear();
                continue;
            }

            for (unsigned p{ 0 }; p < face.Size(); ++p)
            {
                const Vector3 point{ positions.faces_.At(f).At(p) };
                int neighbourCount{};
                Vector3 aimFrom{ Vector3::ZERO };

                for (int xO{ 0 }; xO < 2; ++xO)
                    for (int yO{ 0 }; yO < 2; ++yO)
                        for (int zO{ 0 }; zO < 2; ++zO)
                        {
                            const IntVector3 offPoint{ xO, yO, zO };
                            const IntVector3 flooredPoint{ VectorFloorToInt(point) };
                            if (blocks_.Contains(coords + flooredPoint + offPoint))
                            {
                                aimFrom += Vector3{ xO, yO, zO } - VectorCeil(point);
                                ++neighbourCount;
                            }
                        }

                aimFrom /= neighbourCount;

                if (aimFrom.LengthSquared() != 0.0f)
                    normals.faces_.At(f).At(p) = -(aimFrom + point).Normalized();
            }
        }

        sect.Push(Geonomicon::Polyhedrune(positions, normals));
        blockModel->SetModel(Grimoire::Summon(sect, 5));
        blockModel->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Block.xml"));
    }

    requiresUpdate_.Clear();
}
