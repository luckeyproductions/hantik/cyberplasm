include(Cyberplasm/Cyberplasm.pri)

DESTDIR = lib
TEMPLATE = lib

CONFIG  += c++11 staticlib
CONFIG  -= qt app_bundle
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

unix:!android: target.path = /usr/lib
!isEmpty(target.path): INSTALLS += target
