INCLUDEPATH += \
    Cyberplasm/include

HEADERS += \
    Cyberplasm/include/block.h \
    Cyberplasm/include/cyberplasm.h \
    Cyberplasm/include/geonomicon.h \
    Cyberplasm/include/grimoire.h \
    Cyberplasm/include/luckey.h \
    Cyberplasm/include/paper.h \
    Cyberplasm/include/rock.h \
    Cyberplasm/include/witch.h

SOURCES += \
    Cyberplasm/src/block.cpp \
    Cyberplasm/src/cyberplasm.cpp \
    Cyberplasm/src/geonomicon.cpp \
    Cyberplasm/src/grimoire.cpp \
    Cyberplasm/src/luckey.cpp \
    Cyberplasm/src/paper.cpp \
    Cyberplasm/src/rock.cpp \
    Cyberplasm/src/witch.cpp
