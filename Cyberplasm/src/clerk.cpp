#include "geonomicon.h"
#include "clerk.h"

using namespace Witch;

void Clerk::RegisterObject(Context* context)
{
    context->RegisterFactory<Clerk>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

Clerk::Clerk(Context* context): LogicComponent(context),
    drawNode_{ nullptr },
    currentCallis_{ nullptr },
    hover_{ nullptr },
    cursorPos_( Vector3::ONE * M_INFINITY ),
    drawDistance_{},
    startRadius_{ 10.0f },
    radius_{},
    color_( Color::RED ),
    material_{ nullptr },
    dry_{},
    wet_{}
{
}

void Clerk::OnNodeSet(Node* node)
{
    if (node)
    {
        material_ = CACHE->GetResource<Material>("Materials/Ooze.xml")->Clone();
        material_->SetShaderParameter("MatDiffColor", color_);
        drawNode_ = node_->CreateChild("Draw");
        SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Clerk, DrawCursor));
    }
    else
        UnsubscribeFromEvent(E_POSTRENDERUPDATE);
}


void Clerk::Update(float timeStep)
{
    Input* input{ GetSubsystem<Input>() };

    if (input->GetMouseMoveWheel() != 0)
    {
        float hue{ color_.Hue() + input->GetMouseMoveWheel() / 60.0f};
        if (hue > 1.0f)
            hue -= 1.0f;
        else if (hue < 0.0f)
            hue += 1.0f;
        color_.FromHSV(hue, color_.SaturationHSV(), color_.Value());

        ColorChanged();
    }

    if (input->GetMouseButtonPress(MOUSEB_LEFT) && !currentCallis_)
    {
        Node* camNode{ GetScene()->GetChild("Camera") };

        currentCallis_ = drawNode_->CreateComponent<Cyberplasm>();
        currentCallis_->SetCastShadows(true);
        currentCallis_->SetMaterial(material_);

        drawDistance_ = cursorPos_.DistanceToPoint(camNode->GetWorldPosition());

        radius_ = startRadius_;

        dry_.start_ = Rune{ cursorPos_ - cursorDirection_ * radius_, -cursorDirection_ };
        wet_.end_   = Rune{ cursorPos_ + cursorDirection_ * radius_,  cursorDirection_ };

        dry_.path_.Push(Rune{ cursorPos_, cursorDirection_.CrossProduct(camNode->GetRight()) });
        dry_.pattern_.Push(Geonomicon::Ellipse(Vector2::ONE * radius_));
        wet_.path_.Push(Rune{ cursorPos_, cursorDirection_.CrossProduct(camNode->GetRight()) });
        wet_.pattern_.Push(Geonomicon::Ellipse(Vector2::ONE * radius_));
    }

    if (!currentCallis_)
    {
        UpdateCursor();

        if (hover_)
        {
            if (input->GetMouseButtonPress(MOUSEB_RIGHT))
            {
                color_ = hover_->GetMaterial()->GetShaderParameter("MatDiffColor").GetColor();
                ColorChanged();
            }
            else if (input->GetKeyPress(KEY_X))
            {
                hover_->Remove();
                hover_ = nullptr;
            }
        }
    }
    else
        DrawCallis(timeStep, !input->GetMouseButtonDown(MOUSEB_LEFT) || radius_ < cbrt(startRadius_));
}

void Clerk::ColorChanged()
{
    material_ = material_->Clone();
    material_->SetShaderParameter("MatDiffColor", color_);
}

void Clerk::DrawCallis(float timeStep, bool endDraw)
{
    if (!endDraw)
    {
        Node* camNode{ GetScene()->GetChild("Camera") };
        Vector3 oldPos{ cursorPos_ };
        cursorPos_ = camNode->GetWorldPosition() + camNode->GetDirection() * drawDistance_;
        Vector3 cursorOffset{ cursorPos_ - oldPos };
        Vector3 direction{ cursorOffset.Normalized() };
        float cursorDelta{ cursorOffset.Length() };
        float cursorAngle{ cursorDirection_.Angle(direction) };

        cursorDirection_ = Quaternion::IDENTITY.Slerp(Quaternion{ cursorDirection_, direction }, Min(timeStep * Sqrt(cursorAngle) * cursorDelta, 1.5f))
                           * cursorDirection_;

        const Vector3 normal{ cursorDirection_.CrossProduct(direction).CrossProduct(direction) };

        wet_.path_.Back() = Rune{ cursorPos_, normal };
        wet_.path_.Front() = Wand{ dry_.path_.Back(), wet_.path_.Back() }.Plot(3/4.f, normal);
        Vector3 tipDirection{ (wet_.path_.Back().Position() - wet_.path_.Front().Position()).Normalized() };
        wet_.end_ = Rune{ cursorPos_ + cursorDirection_ * radius_, cursorDirection_ };

        Rune fromDry{ (dry_.path_.Size() < 2 ? dry_.start_ : dry_.path_.At(dry_.path_.Size() - 2)) };
        Vector3 pathDirection{ (dry_.path_.Back().Position() - fromDry.Position()).Normalized() };
        cursorAngle = (cursorDirection_.Angle(direction) + 3.0f * cursorDirection_.Angle(pathDirection)) * 0.25f;
        float fromPath{ dry_.path_.Back().Position().DistanceToPoint(cursorPos_) };

        if (fromPath > cbrt(radius_ / sqrt(0.01f * cursorAngle + 1.0f)) * Pow(startRadius_, M_PHI) / M_PHI
            || Sqrt(cursorAngle + Min(radius_, normal.Angle(wet_.path_.Back().Normal()))) > 9.0f)
        {
            radius_ -= Sqrt(fromPath) * 0.1f;

            if (radius_ >= cbrt(startRadius_))
            {
                dry_.path_.Push(wet_.path_.Back());
                dry_.pattern_.Push(wet_.pattern_.Back());

                wet_.pattern_.Front() = wet_.pattern_.Back();
                wet_.pattern_.Back() = Geonomicon::Ellipse(Vector2::ONE * radius_);
            }
        }

        dry_.end_ = Rune{ wet_.path_.Front().Position() + pathDirection * radius_ * M_PHI, pathDirection };
    }


    Spell fullPath{ dry_.path_ };
    fullPath.Push( wet_.path_ );
    Sect fullPattern{ dry_.pattern_ };
    fullPattern.Push(wet_.pattern_);

    currentCallis_->Generate(Geonomicon::Callis(fullPath, fullPattern, dry_.start_, endDraw ? dry_.end_ : wet_.end_), 1u + Round((1.0f + 0.05f * startRadius_) * endDraw));

    if (endDraw)
    {
        currentCallis_ = nullptr;
        wet_ = dry_ = CallisData{};
    }
}

void Clerk::UpdateCursor()
{
    Node* camNode{ GetScene()->GetChild("Camera") };
    const Ray ray{ camNode->GetWorldPosition(), camNode->GetDirection() };
    PODVector<RayQueryResult> result{};
    RayOctreeQuery query{ result, ray };
    GetScene()->GetComponent<Octree>()->RaycastSingle(query);

    if (result.Size())
    {
        RayQueryResult r{ result.Front() };
        cursorPos_ = r.position_;
        cursorDirection_ = r.normal_;

        if (r.drawable_->GetNode() == drawNode_)
            hover_ = static_cast<Cyberplasm*>(r.drawable_);
    }
    else
    {
        cursorPos_ = Vector3::ONE * M_INFINITY;
        cursorDirection_ = Vector3::ZERO;
        hover_ = nullptr;
    }
}

void Clerk::DrawCursor(StringHash eventType, VariantMap& eventData)
{
    if (!currentCallis_ && cursorPos_.x_ != M_INFINITY)
        GetScene()->GetComponent<DebugRenderer>()->AddCross(cursorPos_ - GetScene()->GetChild("Camera")->GetDirection() * 5.0f, 10.0f, color_);
}
