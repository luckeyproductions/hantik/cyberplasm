#include "rock.h"

using namespace Witch;

void Rock::RegisterObject(Context* context)
{
    context->RegisterFactory<Rock>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

Rock::Rock(Context* context): LogicComponent(context),
    cyberplasm_{ nullptr },
    speed_{ 0.55f },
    rockTime_{},
    refRunes_{},
    offRunes_{},
    onRunes_{}
{
}

void Rock::Start()
{
}

void Rock::OnNodeSet(Node* node)
{
    if (!node)
        return;

    
    cyberplasm_ = node_->CreateComponent<Cyberplasm>();
    cyberplasm_->SetCastShadows(true);
    cyberplasm_->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Rock.xml"));

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    CollisionShape* cs{ node_->CreateComponent<CollisionShape>()};
    rb->SetFriction(0.8f);
    rb->SetAngularDamping(0.05f);
    rb->SetRestitution(0.2f);

    Randomize();
}

void Rock::Randomize()
{
    const int n{ Random(3, 6) };
    const float a{ 360.0f / n };
    const float r{ Min(10.0f, Max(0.25f, Pow(RandomNormal(1.0f, 0.5f), 2.0f))) };

    refRunes_.Push(Rune{ { 0.0f, r * Random(0.25f, 4.0f), 0.0f }, Vector3::UP });

    for (int i{ 0 }; i < n; ++i)
    {
        Vector3 pos{ Sin(a * i) * r * Random(0.5f, 2.0f), r * Random(-0.5f, 0.5f), Cos(a * i) * r * Random(0.5f, 2.0f) };
        Vector3 norm{ pos.ProjectOntoPlane(Vector3::UP).Normalized() };

        refRunes_.Push(Rune{ pos, norm });
    }

    refRunes_.Push(Rune{ { 0.0f, -r * Random(0.25f, 4.0f), 0.0f }, Vector3::DOWN });

    for (int o{ 0 }; o < refRunes_.Size(); ++o)
    {
        Vector3 offset{ Vector3{ Random(-0.875f, 0.875f),
                        Random(-0.875f, 0.875f),
                        Random(-0.875f, 0.875f) } };
        Vector3 normal{ (refRunes_[o].Normal() + offset * 0.5f).Normalized() };
        offset += offset.ProjectOntoPlane(normal) * Sqrt(offset.Length());

        refRunes_[o] = Rune{ refRunes_[o].Position() + (refRunes_[o].Position() + offset + normal).Normalized() * r, normal };
    }

    Spell spell{};
    float d{ 5.0f};

    for (int i{ 0 }; i < n; ++i)
    {
        Spell triRune{ { refRunes_.Front(),
                        refRunes_.At(1 + (i % n)),
                        refRunes_.At(1 + ((i + 1) % n)) } };
        spell.Push(triRune);

        Spell triRuneM{ refRunes_.Back(), triRune.At(2), triRune.At(1) };
        spell.Push(triRuneM);
    }

    RigidBody* rb{ node_->GetComponent<RigidBody>() };
    rb->SetMass(2.0f * Pow(r, 3.0f));

    cyberplasm_->Generate(Grimoire::Tris(spell), 1);
//    const BoundingBox& bb{ cyberplasm_->GetBoundingBox() };
    node_->GetComponent<CollisionShape>()->SetConvexHull(cyberplasm_->GetModel());
    cyberplasm_->Generate(Grimoire::Tris(spell), 1 + Ceil(r / M_PHI));

    node_->Rotate(Quaternion{ Random(360.0f), Random(360.0f), Random(360.0f) });
}
