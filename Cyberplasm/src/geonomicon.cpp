#include "geonomicon.h"

using namespace Witch;

Spell Geonomicon::Rectangle(const Vector2& size, const Vector3& normal, const Vector2& offset)
{
    const Vector2 halfSize{ size * 0.5f };
    const Vector3 offset3d{ offset.x_, offset.y_, 0.0f };

    Spell rect{{}, Spell::QUAD };
    for (unsigned c{ 0 }; c < 4; ++c)
    {
        const Vector3 pos{ halfSize.x_ * (c % 2 ? 1.0f : -1.0f ), halfSize.y_ * (c / 2 ? -1.0f : 1.0f ), 0.0f };
        rect.Push(Rune{ { offset3d + pos }, { normal.Length() == 0.0f ? pos.Normalized() : normal } });
    }

    return rect;
}

Spell Geonomicon::Ellipse(const Vector2& size, unsigned n, const Vector2& offset)
{
    if (n == 0)
        return {{}, Spell::RING };

    const Vector2 halfSize{ size * 0.5f };
    const Vector3 offset3d{ offset.x_, offset.y_, 0.0f };

    const float th{ 360.0f / n };

    Spell ring{{}, Spell::RING };
    for (unsigned r{ 0 }; r < n; ++r)
    {
        const Vector3 pos{ Vector3{ -halfSize.x_ * Sin(r * th), halfSize.y_ * Cos(r * th), 0.0f } };
        ring.Push(Rune{ { offset3d + pos }, { pos.Normalized() } });
    }

    return ring;
}

Sect Geonomicon::Block(const Vector3& size, float radius)
{
    Sect sect{};
    Spell scroll{};
    radius = 0.5f * Min(size.x_, Min(size.y_, Min(size.z_, radius * 2.0f)));

    if (radius > 0.0f)
    {
        const Vector3 cornerPos{ size * 0.5f - Vector3::ONE * radius };
        for (int c{ 0 }; c < 8; ++c)
        {
            Spell scribble{};
            Matrix3x4 mPos{};
            Matrix3x4 mScale{};
            Vector3 scale{ 1.0f - 2.0f * (c % 2),
                         1.0f - 2.0f * (c / 2 % 2),
                         1.0f - 2.0f * (c / 4) };
            mScale.SetScale(scale);

            mPos.SetTranslation(mScale * cornerPos);

            for (int v{ 0 }; v < 3; ++v)
            {
                Quaternion rot{ (v == 0 ? 0.0f : 90.0f), (v == 1 ? Vector3::RIGHT : Vector3::BACK) };
                Matrix3x4 mRot{ rot.RotationMatrix() };

                scribble.Push(Rune{ Vector3::UP * radius, Vector3::UP }.Transformed(mRot).Transformed(mScale).Transformed(mPos));
            }

            float scaleSign{ Sign(scale.x_) * Sign(scale.y_) * Sign(scale.z_)};
            if (scaleSign < 0.0f)
                scribble.Reverse();

            scroll.Push(scribble);
        }
        sect.Push(Grimoire::Tris(scroll));

        scroll.Clear();
        Vector3 edgePos{ size.ProjectOntoPlane(Vector3::FORWARD) * 0.5f - Vector3{ 1.0f, 1.0f, 0.0f } * radius };
        for (int e{ 0 }; e < 12; ++e)
        {

            if (e == 4)
                edgePos.x_ = size.z_ * 0.5f - radius;
            if (e == 8)
                edgePos.y_ = size.x_ * 0.5f - radius;

            Spell scribble{};
            Matrix3x4 mPos{};
            Matrix3x4 eRot{ Quaternion{ 0.0f, e / 4 * 90.0f, 0.0f }.RotationMatrix() };
            Matrix3x4 lRot{ Quaternion{ e / 8 * 90.0f, 0.0f, e / 8 * 90.0f }.RotationMatrix() };
            Matrix3x4 mScale{};
            Vector3 scale{ 1.0f - 2.0f * (e % 2),
                           1.0f - 2.0f * (e / 2 % 2),
                           1.0f };
            mScale.SetScale(scale);

            mPos.SetTranslation(Quaternion{ 0.0f, e / 4 * 90.0f, 0.0f } * (mScale * edgePos));

            for (int v{ 0 }; v < 4; ++v)
            {
                Matrix3x4 vRot{ Quaternion{ (v % 2 != v / 2 ? 90.0f : 0.0f), Vector3::BACK }.RotationMatrix() };
                float z{ (v / 2 == 0 ? 1.0f : -1.0f) *
                         ((e < 4 ? size.z_ : (e < 8 ? size.x_ : size.y_)) * 0.5f - radius )};

                scribble.Push(Rune{ Vector3::UP * radius + Vector3::FORWARD * z, Vector3::UP }.Transformed(vRot).Transformed(mScale).Transformed(eRot).Transformed(mPos).Transformed(lRot));
            }

            float scaleSign{ Sign(scale.x_) * Sign(scale.y_) * Sign(scale.z_)};
            if (scaleSign < 0.0f)
                scribble = { scribble.At(3), scribble.At(2), scribble.At(1), scribble.At(0) };

            scroll.Push(scribble);
        }
        sect.Push(Grimoire::Quads(scroll));
    }

    scroll.Clear();
    for (int f{ 0 }; f < 6; ++f)
    {
        Spell scribble{};

        float y{ 0.5f * size.y_ };
        if (f > 3)
            y = 0.5f * size.x_;
        else if (f > 1)
            y = 0.5f * size.z_;

        Vector3 pv{ 0.5f * (f < 4 ? size.x_: size.y_) - radius,
                    y,
                    0.5f * (f < 2 || f >= 4 ? size.z_ : size.y_) - radius };

        if (pv.x_ == 0.0f || pv.y_ == 0.0f || pv.z_ == 0.0f)
            continue;

        for (int v{ 0 }; v < 4; ++v)
        {
            Matrix3x4 mv{};
            Vector3 scale{ -2.0f * (v % 2 != v / 2) + 1.0f,
                           -1.0f,
                           1.0f - 2.0f * (v / 2) };
            Quaternion rot{ f % 2 * 180.0f - (f < 2 ? 0.0f : 90.0f), (f > 3 ? Vector3::FORWARD : Vector3::RIGHT) };
            mv.SetScale(scale);
            scribble.Push(Rune{ pv, Vector3::UP }.Transformed(mv).Transformed(Matrix3x4{ rot.RotationMatrix() }));
        }

        scroll.Push(scribble);
    }
    sect.Push(Grimoire::Quads(scroll));

    return sect;
}

Sect Geonomicon::Torus(float rIn, float rOut, unsigned n, unsigned m)
{
    if (rOut <= 0.0f)
        return {};

    const float rA{ (rIn - rOut) * 0.5f };
    const float rB{ (rIn + rOut) * 0.5f };
    const float dn{ (n != 0 ? 360.0f / n : 0.0f) };
    const float dm{ (m != 0 ? 360.0f / m : 0.0f) };

    Spell scroll{};

    if (rIn >= rOut)
    {
        for (unsigned j{ 0 }; j < m; ++j)
        {
            float am{ j * dm };
            Quaternion rm{ am, Vector3::UP };
        }

        Sect sect{ Grimoire::Rings(scroll, m) };

        return sect;
    }


    for (unsigned j{ 0 }; j < m; ++j)
    {
        float am{ j * dm };
        Quaternion rm{ am, Vector3::UP };

        for (unsigned i{ 0 }; i < n; ++i)
        {
            float an{ i * dn };
            Quaternion rn{ an, Vector3::RIGHT };
            Vector3 locPos{ rn * Vector3::FORWARD * rA };

            scroll.Push(Rune{ rm * (locPos + Vector3::FORWARD * rB), rm * locPos.Normalized() });
        }
    }

    Sect sect{ Grimoire::Rings(scroll, n) };
    sect.Push(sect.Front());

    return Grimoire::Weave(sect);
}

Sect Geonomicon::Polyhedrune(const Polyhedron& position, const Polyhedron& normal)
{
    Sect sect{};
    Vector3 center{};

    //Calculate center to determine missing normals
    for (unsigned f{ 0 }; f < position.faces_.Size(); ++f)
    {
        const PODVector<Vector3> face{ position.faces_.At(f) };

        if (face.Size() < 3)
            continue;

        Vector3 faceCenter{};

        for (unsigned v{ 0 }; v < face.Size(); ++v)
        {
            faceCenter += face.At(v);
        }

        center += faceCenter / face.Size();
    }
    center /= position.faces_.Size();

    for (unsigned f{ 0 }; f < position.faces_.Size(); ++f)
    {
        const PODVector<Vector3> face{ position.faces_.At(f) };

        if (face.Size() < 2)
            continue;

        PODVector<Vector3> vertexNormals{};

        if (normal.faces_.Size() > f)
            vertexNormals = normal.faces_.At(f);

        const bool radialNormals{ vertexNormals.Size() != face.Size() };

        Spell scroll{};

        for (unsigned v{ 0 }; v < face.Size(); ++v)
        {
            if (radialNormals)
                scroll.Push(Rune{ face.At(v), (face.At(v) - center).NormalizedOrDefault(Vector3::UP) });
            else
                scroll.Push(Rune{ face.At(v), vertexNormals.At(v) });
        }

        switch (face.Size())
        {
        default: /*Weave to center*/ continue;
        case 2: sect.Push(Grimoire::Curve(scroll)); break;
        case 3: sect.Push(Grimoire::Tris( scroll)); break;
        case 4: sect.Push(Grimoire::Quads(scroll)); break;
        }
    }

    return sect;
}

Sect Geonomicon::Callis(const Spell& path, const Sect& pattern, const Rune& start, const Rune& end)
{
    Sect sect{};

    unsigned facets{};
    for (const Spell& p: pattern)
        facets = Max(facets, (p.Type() == Spell::CURVE ? p.Size() - 1u : p.Size()));

    if (!facets)
        return sect;

    if (start.Position().x_ != M_INFINITY && path.Size() != 0)
    {
        sect.Push({ start });
    }

    if (path.Size() > 0)
    {
        for (unsigned p{ 0 }; p < path.Size(); ++p)
        {
            unsigned np{ Min(path.Size() - 1u, p + 1)};
            unsigned lp{ (p == 0 ? 0 : p - 1u) };

            const Vector3 direction{ (path.At(np).Position()
                                    - path.At(lp).Position()) };

            Quaternion nodeRot{};
            nodeRot.FromLookRotation(direction, path.At(p).Normal());
            Matrix3x4 nodeTransform{};
            nodeTransform.SetRotation(nodeRot.RotationMatrix());
            nodeTransform.SetTranslation( path.At(p).Position() );

            const Spell& segment{ pattern.At(p % pattern.Size()).Transformed(nodeTransform) };

            sect.Push(segment);
        }
    }

    if (end.Position().x_ != M_INFINITY && path.Size() != 0)
    {
        sect.Push({ end });
    }

    return Grimoire::Weave(sect);
}
