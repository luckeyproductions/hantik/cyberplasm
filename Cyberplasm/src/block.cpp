#include "block.h"

using namespace Witch;

void Block::RegisterObject(Context* context)
{
    context->RegisterFactory<Block>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

Block::Block(Context* context): LogicComponent(context),
    cyberplasm_{ nullptr },
    size_{ Vector3::ONE },
    radius_{ 0.25f },
    density_{ 1.0f }
{
}

void Block::OnNodeSet(Node* node)
{
    if (!node)
        return;

    cyberplasm_ = node_->CreateComponent<Cyberplasm>();
    cyberplasm_->SetCastShadows(true);
    cyberplasm_->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Rock.xml"));
    cyberplasm_->Generate(Geonomicon::Block(size_, radius_), 2);

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    CollisionShape* cs{ node_->CreateComponent<CollisionShape>()};
    rb->SetFriction(0.8f);
    rb->SetAngularDamping(0.05f);
    rb->SetRestitution(0.2f);
}
