#include "grimoire.h"
#include "witch.h"

using namespace Witch;

Vector<Rune> Wand::Plot(unsigned resolution, const Vector3& edgeNormal)
{
    if (resolution == 0)
        return { first_, second_ };

    Vector<Rune> curve{};
    unsigned numVerts{ resolution + 2 };

    for (unsigned i{ 0 }; i < numVerts; ++i)
    {
        float t{ i * 1.0f / (numVerts - 1) };
        curve.Push(Plot(t, edgeNormal));
    }

    return curve;
}

Rune Wand::Plot(float t, const Vector3& edgeNormal)
{
    if (t <= 0.0f)
        return anode();
    else if (t >= 1.0f)
        return  cathode();

    PODVector<Vector3> segment{};
    for (int i{ 0 }; i < 3; ++i)
    {
        float dt{ (i == 0 ? 0.0f : M_LARGE_EPSILON * (2 * (i - 1) - 1 )) };
        segment.Push(PlotPos(t + dt, edgeNormal));
    }

    Vector3 d{ (segment[2] - segment[1]).Normalized() };
    Vector3 vIn{ anode().Normal().ProjectOntoPlane(direction()).Normalized() };
    Vector3 vOut{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };

    if (vIn.LengthSquared() == 0.0f)
        vIn = edgeNormal.ProjectOntoPlane(direction()).Normalized();
    if (vOut.LengthSquared() == 0.0f)
        vOut = edgeNormal.ProjectOntoPlane(direction()).Normalized();

    Wand normWand{ { vIn, vIn }, { vOut, vOut }};
    Vector3 v{ normWand.PlotPos(t, edgeNormal).Normalized() };
    float slope{ v.DotProduct(d) };

    Vector3 normal{ v * cos(M_PI_2 * slope) - slope * direction() * sin(M_PI * Abs(slope))};
    normal = normal.Orthogonalize(d);

    float at{ Min(1.0f, Max(0.0f, 1.0f - t * 2.0f)) };
    float ct{ Min(1.0f, Max(0.0f, (t - 0.5f) * 2.0f)) };
    at = at * at * at * at;
    ct = ct * ct * ct * ct;
    normal = normal.Lerp(anode().Normal(), at).Lerp(cathode().Normal(), ct); ///Slerp?

    return { segment[0], normal.Normalized() };
}

Vector3 Wand::PlotPos(float t, const Vector3& edgeNormal)
{
    t = Clamp(t, 0.0f, 1.0f);
    float sinPiT{ sin(M_PI * t) };
    float cosPiT{ cos(M_PI * t) };

    float in{ -direction().DotProduct(anode()  .Normal()) };
    float out{ direction().DotProduct(cathode().Normal()) };
    float th{ Lerp(in, out, t) };

    Vector3 vIn{ anode().Normal().ProjectOntoPlane(direction()).Normalized() };
    Vector3 vOut{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };

    if (vIn.LengthSquared() == 0.0f)
        vIn = edgeNormal;
    if (vOut.LengthSquared() == 0.0f)
        vOut = edgeNormal;

    Vector3 v{ Quaternion::IDENTITY.Slerp({ vIn, vOut }, t) * vIn };
    Vector3 lateral{ Lerp(Vector3::ZERO, v * sinPiT / 2, Abs(th)) };
    if (th < 0.0f)
        lateral = -lateral;
    float z{ Lerp(t, (1 - cosPiT) / 2, th * th) };
    float m{ broom().Length() };

    float att{ atan(sqrt(cos(th * M_PI_4)) / M_PI_2) };
    Vector3 delta{ m * (att * lateral + direction() * z) };

    return anode().Position() + delta;
}

Spell Sect::runes()
{
    Spell res{};

    for (auto s: *this)
        res.Push(s);

    return res;
}

SharedPtr<Geometry> Spell::Cast(unsigned resolution, Matrix3x4 transform) const
{
    if (invoke_ == TRI)
        return TriCast(resolution, transform);
    if (invoke_ == QUAD)
        return QuadCast(resolution, transform);
    else
        return CurveCast(resolution, transform);
}

SharedPtr<Geometry> Spell::CurveCast(unsigned resolution, Matrix3x4 transform) const
{
    const unsigned numVertices{ Size() };

    PODVector<float> vertexData{};
    PODVector<unsigned short> indexData{};

    int i{ 0 };
    for (const Rune& r: *this)
    {
        vertexData.Push(r.Position().x_);
        vertexData.Push(r.Position().y_);
        vertexData.Push(r.Position().z_);
        vertexData.Push(r.Normal().x_);
        vertexData.Push(r.Normal().y_);
        vertexData.Push(r.Normal().z_);

        indexData.Push(i++);
    }

    return Grimoire::Conjure(vertexData, indexData, LINE_STRIP);
}

SharedPtr<Geometry> Spell::TriCast(unsigned resolution, Matrix3x4 transform) const
{
    if (resolution < 0)
        resolution = 0;

    Vector3 faceNormal{ FaceNormal() };
    PODVector<float> vertexData{};
    PODVector<unsigned short> indexData{};
    int i{ 0 };

    Wand highWand{ At(0), At(0) };

    for (unsigned s{ 0 }; s <= resolution; ++s)
    {
        const float t0{ (1.0f + s) / (1.0f + resolution) };
        Wand lowWand{ Wand{ At(0), At(1) }.Plot(t0, faceNormal), Wand{ At(0), At(2) }.Plot(t0, faceNormal) };

        for (unsigned h{ 0 }; h <= 2 * s; ++h)
        {
            bool inverted{ h % 2 != 0 };
            Rune v0, v1, v2;

            for (int v { 0 }; v < 3; ++v)
            {
                bool high{ (inverted ? v < 2 : v == 0)};
                float t1{ 0.0f };

                if (!inverted)
                {
                    if (high)
                        t1 = 0.5f * h / Max(1, s);
                    else
                        t1 = (0.5f * h + (v == 1)) / (1 + s);
                }
                else
                {
                    if (high)
                        t1 = (0.5f * (h - 1) + (v == 1)) / s;
                    else
                        t1 = 0.5f * (h + 1) / (s + 1);
                }

                Rune vertex;

                if (high)
                    vertex = highWand.Plot(t1, faceNormal);
                else
                    vertex = lowWand.Plot(t1, faceNormal);

                if (transform != Matrix3x4::IDENTITY)
                    vertex = vertex.Transformed(transform);

                switch (v) {
                case 0: v0 = vertex; break;
                case 1: v1 = vertex; break;
                case 2: v2 = vertex; break;
                default: break;
                }
            }

            for (const Rune& r: { v0, v2, v1 })
            {
                vertexData.Push(r.Position().x_);
                vertexData.Push(r.Position().y_);
                vertexData.Push(r.Position().z_);
                vertexData.Push(r.Normal().x_);
                vertexData.Push(r.Normal().y_);
                vertexData.Push(r.Normal().z_);

                indexData.Push(i++);
            }
        }

        highWand = lowWand;
    }

    return Grimoire::Conjure(vertexData, indexData, TRIANGLE_LIST);
}

SharedPtr<Geometry> Spell::QuadCast(unsigned resolution, Matrix3x4 transform) const
{
    if (resolution < 0)
        resolution = 0;

    const Vector3 faceNormal{ FaceNormal() };
    PODVector<float> vertexData{};
    PODVector<unsigned short> indexData{};
    int i{ 0 };

    Wand highWand{ At(0), At(1) };

    for (unsigned s{ 0 }; s <= resolution; ++s)
    {
        const float t0{ (1.0f + s) / (1.0f + resolution) };
        Wand lowWand{ Wand{ At(0), At(3) }.Plot(t0, faceNormal),
                      Wand{ At(1), At(2) }.Plot(t0, faceNormal) };

        for (unsigned h{ 0 }; h <= resolution; ++h)
        {
            for (bool inverted: { false, true })
            {
                Rune v0, v1, v2;

                for (int v { 0 }; v < 3; ++v)
                {
                    float t1;

                    if (inverted)
                        t1 = (h + (v != 0)) / (resolution + 1.0f);
                    else
                        t1 = (h + (v == 2)) / (resolution + 1.0f);

                    bool high{ (inverted ? v != 1 : v == 0) };
                    Rune vertex;

                    if (high)
                        vertex = highWand.Plot(t1, faceNormal);
                    else
                        vertex = lowWand.Plot(t1, faceNormal);

                    if (transform != Matrix3x4::IDENTITY)
                        vertex = vertex.Transformed(transform);

                    switch (v)
                    { default: break;
                    case 0: v0 = vertex; break;
                    case 1: v1 = vertex; break;
                    case 2: v2 = vertex; break;
                    }
                }

                for (const Rune& r: { v0, v2, v1 })
                {
                    vertexData.Push(r.Position().x_);
                    vertexData.Push(r.Position().y_);
                    vertexData.Push(r.Position().z_);
                    vertexData.Push(r.Normal().x_);
                    vertexData.Push(r.Normal().y_);
                    vertexData.Push(r.Normal().z_);

                    indexData.Push(i++);
                }
            }
        }

        highWand = lowWand;
    }

    return Grimoire::Conjure(vertexData, indexData, TRIANGLE_LIST);
}

Vector3 Spell::FaceNormal(int i) const
{
    switch (invoke_)
    {
    case POINTS: case CURVE: default:
    {
        return Vector3::ZERO;
    }
    case TRI:
    {
        Vector3 edge1{ At(0 + i * 3).Position() - At(1 + i * 3).Position() };
        Vector3 edge2{ At(0 + i * 3).Position() - At(2 + i * 3).Position() };
        return edge1.CrossProduct(edge2).Normalized();
    }
    case QUAD:
    {
        const Vector3 edge1{ At(0 + i * 4).Position() - At(1 + i * 4).Position() };
        const Vector3 edge2{ At(1 + i * 4).Position() - At(3 + i * 4).Position() };
        const Vector3 edge3{ At(0 + i * 4).Position() - At(3 + i * 4).Position() };
        const Vector3 edge4{ At(3 + i * 4).Position() - At(2 + i * 4).Position() };
        return (edge1.CrossProduct(edge2).Normalized() + edge3.CrossProduct(edge4).Normalized()).Normalized();
    }
    }
}

