#include "paper.h"

using namespace Witch;

void Paper::RegisterObject(Context* context)
{
    context->RegisterFactory<Paper>();
    DRY_COPY_BASE_ATTRIBUTES(LogicComponent);
}

Paper::Paper(Context* context): LogicComponent(context),
    cyberplasm_{ nullptr },
    speed_{ 0.55f },
    paperTime_{},
    refRunes_{},
    offRunes_{},
    onRunes_{}
{
}

void Paper::Start()
{
}

void Paper::Update(float timeStep)
{
    RigidBody* rb{ GetComponent<RigidBody>() };
    float damp{ 0.1f + node_->GetUp().DotProduct(rb->GetLinearVelocity().Normalized()) * 0.8f };
    damp *= damp;
    rb->SetLinearDamping(damp);
}

void Paper::OnNodeSet(Node *node)
{
    if (!node)
        return;

    cyberplasm_ = node_->CreateComponent<Cyberplasm>();
    cyberplasm_->SetCastShadows(true);
    cyberplasm_->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Paper.xml"));

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    CollisionShape* cs{ node_->CreateComponent<CollisionShape>()};
    rb->SetFriction(0.9f);
    rb->SetAngularDamping(0.75f);
    rb->SetLinearDamping(0.9f);
    rb->SetLinearRestThreshold(0.5f);
    rb->SetAngularRestThreshold(0.5f);

    Randomize();
}

void Paper::Randomize()
{
    const int n{ 4 };
    const float a{ 360.0f / n };
    const float r{ 4.20f * Min(1.5f, Max(0.25f, RandomNormal(0.9f, 0.05f))) };

    for (int i{ 0 }; i < n; ++i)
    {
        Vector3 pos{ Quaternion{ a * i - 45.0f, Vector3::UP} * Vector3::FORWARD * r };
        Vector3 norm{ Vector3::UP };

        refRunes_.Push(Rune{ pos, norm });
    }

    for (int o{ 0 }; o < refRunes_.Size(); ++o)
    {
        Vector3 offset{ Vector3{ Random(-0.75f, 0.75f),
                        Random(-0.5f, 0.5f),
                        Random(-0.75f, 0.75f) } };
        Vector3 normal{ (refRunes_[o].Normal() + offset).Normalized() };
//        offset += offset.ProjectOntoPlane(normal) * Pow(offset.Length(), 3.0f);

        refRunes_[o] = Rune{ refRunes_[o].Position() + offset.DotProduct(Vector3::UP) * normal * r * 0.25f, normal };
    }

    Spell spell{ refRunes_.At(0), refRunes_.At(1), refRunes_.At(2), refRunes_.At(3) };

    RigidBody* rb{ node_->GetComponent<RigidBody>() };
    rb->SetMass(0.01f * Pow(r, 2.0f));
    rb->ApplyImpulse((node_->GetPosition().Normalized() + Vector3::UP) * r * .5f);

//    cyberplasm_->Generate(Grimoire::Quad(spell), Round(r * 0.25f));
//    node_->GetComponent<CollisionShape>()->SetConvexHull(cyberplasm_->GetModel());
    cyberplasm_->Generate(Grimoire::Quads(spell), Ceil(r) * 0.25f);

    const BoundingBox& bb{ cyberplasm_->GetBoundingBox() };
    node_->GetComponent<CollisionShape>()->SetBox(bb.Size(), bb.Center());//Sphere(refRunes_.At(0).Position().Length() * 2.0f);

    node_->Rotate(Quaternion{ Random(360.0f), Random(360.0f), Random(360.0f) });
}
