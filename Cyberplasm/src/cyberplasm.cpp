#include <Dry/Graphics/Geometry.h>
#include <Dry/Graphics/IndexBuffer.h>
#include <Dry/Graphics/VertexBuffer.h>

#include "cyberplasm.h"

using namespace Witch;

Cyberplasm::Cyberplasm(Context* context): StaticModel(context),
    material_{ nullptr }
{
}

void Cyberplasm::RegisterObject(Context* context)
{
    context->RegisterFactory<Cyberplasm>("Geometry");

    DRY_COPY_BASE_ATTRIBUTES(StaticModel);
}

void Cyberplasm::Generate(Sect sect, unsigned resolution)
{
    StaticModel::SetModel(Grimoire::Summon(sect, resolution));
    StaticModel::SetMaterial(material_);
}
