#ifndef CYBERPLASM_H
#define CYBERPLASM_H

#include <Dry/Graphics/StaticModel.h>

#include "grimoire.h"

class Cyberplasm: public StaticModel
{
    DRY_OBJECT(Cyberplasm, StaticModel);

public:
    Cyberplasm(Context* context);
    static void RegisterObject(Context* context);

    void SetMaterial(Material* material) override
    {
        if (material_ == material)
            return;

        material_ = material;
        StaticModel::SetMaterial(material_);
    }

    void Generate(Witch::Sect sect, unsigned resolution = 9);

private:
    SharedPtr<Material> material_;
};

#endif // CYBERPLASM_H
