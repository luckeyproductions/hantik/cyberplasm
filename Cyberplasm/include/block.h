#ifndef BLOCK_H
#define BLOCK_H

#include "cyberplasm.h"
#include "geonomicon.h"

class Block: public LogicComponent
{
    DRY_OBJECT(Block, LogicComponent);

public:
    Block(Context* context);
    static void RegisterObject(Context* context);

    void SetSize(const Vector3& size, float radius = M_INFINITY)
    {
        bool changed{ false };
        if (size_ != size)
        {
            size_ = size;
            radius_ = 0.5f * Min(size_.x_, Min(size_.y_, Min(size_.z_, radius_ * 2.0f)));

            changed = true;
        }
        if (radius != M_INFINITY && radius_ != radius)
        {
            radius_ = 0.5f * Min(size_.x_, Min(size_.y_, Min(size_.z_, radius * 2.0f)));
            changed = true;
        }

        if (changed)
        {
            cyberplasm_->Generate(Geonomicon::Block(size_, radius_), Ceil(radius_));

            CollisionShape* cs{ GetComponent<CollisionShape>() };
            cs->SetBox(size_);

            float volume{ size_.x_ * size.y_ * size.z_ - Pow(radius_, 3.0f) };
            GetComponent<RigidBody>()->SetMass(volume * density_);
        }
    }

protected:
    void OnNodeSet(Node* node) override;

private:
    Witch::Sect Mass() const;
    Cyberplasm* cyberplasm_;

    Vector3 size_;
    float radius_;
    float density_;
};

#endif // BLOCK_H
