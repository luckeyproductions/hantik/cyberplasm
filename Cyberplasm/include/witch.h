#ifndef WITCH_H
#define WITCH_H

#include <initializer_list>

#include "luckey.h"

namespace Witch
{

struct Rune: public Pair<Vector3, Vector3>
{
    using Pair::Pair;

    Rune(const Vector3& position = Vector3::ZERO, const Vector3& normal = Vector3::UP): Pair<Vector3, Vector3>(position, normal.Normalized()) {}
    Vector3 Position() const { return first_; }
    Vector3 Normal() const { return second_; }

    Rune Transformed(Matrix3x4 transform) const
    {
//        Vector3 scale{ transform.Scale() };
//        float scaleSign{ Sign(scale.x_) * Sign(scale.y_) * Sign(scale.z_)};

        return { transform * Position(),
                (transform.ToMatrix3() * Normal()).Normalized() };
    }
};

struct Wand: Pair<Rune, Rune>
{
    using Pair::Pair;

    Vector<Rune> Plot(unsigned resolution, const Vector3& edgeNormal = Vector3::ZERO);

    const Rune& anode() const { return first_ ; }
    const Rune& cathode() const { return second_ ; }
    Vector3 broom() const { return cathode().Position() - anode().Position(); }
    Vector3 direction() const { return broom().Normalized(); }

    Rune Plot(float t, const Vector3& edgeNormal = Vector3::ZERO);
    Vector3 PlotPos(float t, const Vector3& edgeNormal = Vector3::ZERO);
};



class Spell: public Vector<Rune>
{
public:
    enum Invocation{ POINTS = 0, CURVE, TRI, QUAD, RING, SHEET };

    Spell(const std::initializer_list<Rune>& list): Spell(list, POINTS) {}
    Spell(const Vector<Rune>& runes = {}, Invocation type = POINTS): Vector<Rune>(runes),
        invoke_{ type }
    {}
    Invocation Type() const { return invoke_; }

    Vector3 Position(int i) const { return At(Clamp(i, 0, static_cast<int>(Size()) - 1)).Position(); }
    Vector3 Normal(int i)   const { return At(Clamp(i, 0, static_cast<int>(Size()) - 1)).Normal(); }
    SharedPtr<Geometry> Cast(unsigned resolution = 0, Matrix3x4 transform = {}) const;

    void Reverse()
    {
        Spell rev{};
        for (unsigned r{ Size() }; r != 0;)
            rev.Push(At(--r));
        Swap(rev);
    }

    Spell Transformed(Matrix3x4 transform) const
    {
        Spell res{ {}, Type() };
        for (const Rune& r: *this)
            res.Push(r.Transformed(transform));

        return res;
    }

private:
    Invocation invoke_;

    SharedPtr<Geometry> CurveCast(unsigned resolution = 0u, Matrix3x4 transform = {}) const;
    SharedPtr<Geometry> TriCast(  unsigned resolution = 0u, Matrix3x4 transform = {}) const;
    SharedPtr<Geometry> QuadCast( unsigned resolution = 0u, Matrix3x4 transform = {}) const;

    Vector3 FaceNormal(int i = 0) const;
};

class Sect: public Vector<Spell>
{
    using Vector::Vector;
public:
    Spell runes();
};
/*
class Ring: Spell
{
    Spell reference_;

public:
    Ring(int n, float r = 0.5f): Spell(n), reference_(n)
    {
        if (n < 2)
            n = 2;

        const float a{ 360.0f / n };

        for (int i{ 0 }; i < n; ++i)
        {
            Vector3 pos{ Sin(a * i) * r, 0.0f, Cos(a * i) * r };
            Vector3 norm{ pos.ProjectOntoPlane(Vector3::UP).Normalized() };

            reference_.At(i) = At(i) = Rune{ pos, norm };
        }
    }

    SharedPtr<Geometry>  Cast(int resolution = 0, Matrix3x4 transform = {}) const override
    {
//        for (unsigned i{ 0 }; i < Size(); ++i)
//            At(i) = Rune{ transform_ * reference_.At(i).Position(), transform_.Rotation() * reference_.At(i).Normal() };
    }
};

class Sheet: Spell
{
//public:
//    Sheet(const Ring& ringA, const Ring& ringB)
};

*/

};

#endif // WITCH_H
