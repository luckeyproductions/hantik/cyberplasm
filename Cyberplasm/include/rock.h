#ifndef ROCK_H
#define ROCK_H

#include "cyberplasm.h"

class Rock: public LogicComponent
{
    DRY_OBJECT(Rock, LogicComponent);

public:
    Rock(Context* context);
    static void RegisterObject(Context* context);

    void Start() override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void Randomize();

    Node* mess_;
    Cyberplasm* cyberplasm_;
    float speed_;
    Vector<float> rockTime_;

    Vector<Witch::Rune> refRunes_;
    Vector<Witch::Rune> offRunes_;
    Vector<Witch::Rune> onRunes_;
};

#endif // ROCK_H
