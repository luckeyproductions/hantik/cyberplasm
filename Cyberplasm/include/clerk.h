#ifndef CLERK_H
#define CLERK_H

#include "cyberplasm.h"

struct CallisData
{
    Witch::Spell path_{};
    Witch::Sect pattern_{};
    Witch::Rune start_{ Vector3::ONE * M_INFINITY };
    Witch::Rune end_{ Vector3::ONE * M_INFINITY };
};

class Clerk : public LogicComponent
{
    DRY_OBJECT(Clerk, LogicComponent);

public:
    Clerk(Context* context);
    static void RegisterObject(Context* context);

    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;
    void DrawCallis(float timeStep, bool endDraw = false);

private:
    void UpdateCursor();
    void DrawCursor(StringHash eventType, VariantMap& eventData);
    void ColorChanged();

    Node* drawNode_;
    Cyberplasm* currentCallis_;
    Cyberplasm* hover_;

    Vector3 cursorPos_;
    Vector3 cursorDirection_;
    float drawDistance_;
    float startRadius_;
    float radius_;
    Color color_;
    SharedPtr<Material> material_;

    CallisData dry_;
    CallisData wet_;
};

#endif // CLERK_H
