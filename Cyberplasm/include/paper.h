#ifndef PAPER_H
#define PAPER_H

#include "cyberplasm.h"

class Paper: public LogicComponent
{
    DRY_OBJECT(Paper, LogicComponent);

public:
    Paper(Context* context);
    static void RegisterObject(Context* context);

    void Start() override;
    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void Randomize();

    Node* mess_;
    Cyberplasm* cyberplasm_;
    float speed_;
    Vector<float> paperTime_;
    Vector<Witch::Rune> refRunes_;
    Vector<Witch::Rune> offRunes_;
    Vector<Witch::Rune> onRunes_;

    void RenderDebug(StringHash eventType, VariantMap& eventData);
};

#endif // PAPER_H
